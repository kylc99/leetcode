import java.util.*;


/**
 * Main
 *
 * @author by kylc on 06/01/2023
 */

public class TopKFrequentInArray {

    public static void main(String[] args) {
        int[][] testcase = {
                {2, 2, 1, 1, 1, 1, 3, 3},
                {2, 2, 1, 1, 1, 1, 3, 3, 3},
                {4, 1, -1, 2, -1, 2, 3},
                {5, 3, 1, 1, 1, 3, 73, 1},
                {5, 2, 5, 3, 5, 3, 1, 1, 3},
                {5, -3, 9, 1, 7, 7, 9, 10, 2, 2, 10, 10, 3, -1, 3, 7, -9, -1, 3, 3},
                {5, 1, -1, -8, -7, 8, -5, 0, 1, 10, 8, 0, -4, 3, -1, -1, 4, -5, 4, -3, 0, 2, 2, 2, 4, -2, -4, 8, -7, -7, 2, -8, 0, -8, 10, 8, -8, -2, -9, 4, -7, 6, 6, -1, 4, 2, 8, -3, 5, -9, -3, 6, -8, -5, 5, 10, 2, -5, -1, -5, 1, -3, 7, 0, 8, -2, -3, -1, -5, 4, 7, -9, 0, 2, 10, 4, 4, -4, -1, -1, 6, -8, -9, -1, 9, -9, 3, 5, 1, 6, -1, -2, 4, 2, 4, -6, 4, 4, 5, -5}

        };
        int k = 3;
        for (int[] nums : testcase) {
            System.out.println("topKFrequent4: " + Arrays.toString(topKFrequent4(nums, k)));
            System.out.println("topKFrequent3: " + Arrays.toString(topKFrequent3(nums, k)));
            System.out.println("topKFrequent2: " + Arrays.toString(topKFrequent2(nums, k)));
        }
    }

    /**
     * worst solution
     *
     * @param nums
     * @param k
     * @return
     */
    public static int[] topKFrequent2(int[] nums, int k) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        Map<Integer, Integer> frequent = new HashMap<>();
        for (Integer num : nums) {
            frequent.merge(num, 1, Integer::sum);
        }
        frequent.forEach((key, counter) -> {
            if (pq.size() < k) {
                pq.add(key);
            } else {
                int min = pq.stream().min(Comparator.comparing(frequent::get)).orElse(key);
                if (counter > frequent.get(min)) {
                    pq.remove(min);
                    pq.add(key);
                }
            }
        });

        return pq.stream().mapToInt(Integer::intValue).toArray();
    }

    /**
     * best solution: O(n)
     *
     * @param nums
     * @param k
     * @return
     */
    public static int[] topKFrequent4(int[] nums, int k) {
        PriorityQueue<int[]> pq = new PriorityQueue<>(Comparator.comparingInt(a -> a[1]));
        Map<Integer, Integer> frequent = new HashMap<>();
        for (Integer num : nums) {
            frequent.merge(num, 1, Integer::sum);
        }
        frequent.forEach((key, counter) -> {
            if (pq.size() < k) {
                pq.add(new int[]{key, counter});
            } else {
                assert pq.peek() != null;
                if (counter > pq.peek()[1]) {
                    pq.poll();
                    pq.add(new int[]{key, counter});
                }
            }
        });

        return pq.stream().mapToInt(item -> item[0]).toArray();
    }

    /**
     * best solution: O(n)
     *
     * @param nums
     * @param k
     * @return
     */
    public static int[] topKFrequent3(int[] nums, int k) {
        PriorityQueue<Pair> pq = new PriorityQueue<>(new SortCustom());
        Map<Integer, Integer> frequent = new HashMap<>();
        for (Integer num : nums) {
            frequent.merge(num, 1, Integer::sum);
        }
        for (Map.Entry<Integer, Integer> set : frequent.entrySet()) {
            Integer key = set.getKey();
            Integer counter = set.getValue();
            Pair pair = new Pair(key, counter);
            if (pq.size() < k) {
                pq.add(pair);
            } else {
                assert pq.peek() != null;
                if (counter > pq.peek().freq) {
                    pq.poll();
                    pq.add(pair);
                }
            }
        }
        return pq.stream().mapToInt(item -> item.number).toArray();
    }
}

class Pair {
    Integer number;
    int freq;

    Pair(Integer number, int freq) {
        this.number = number;
        this.freq = freq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair pair = (Pair) o;
        return freq == pair.freq && Objects.equals(number, pair.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, freq);
    }

    @Override
    public String toString() {
        return "Pair{" +
                "number=" + number +
                ", freq=" + freq +
                '}';
    }
}

class SortCustom implements Comparator<Pair> {
    public int compare(Pair a, Pair b) {
        return Integer.compare(a.freq, b.freq);
    }
}