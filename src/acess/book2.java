package acess;

import java.lang.reflect.Field;

public class book2 {
    public static void main(String[] args) {
        book1 privateObject = new book1("The Private Value", "ams");
        Field name = null;
        try {
            name = book1.class.getDeclaredField("name");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        name.setAccessible(true);

        String fieldValue = null;
        try {
            fieldValue = (String) name.get(privateObject);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        System.out.println("fieldValue = " + fieldValue);
    }
}
