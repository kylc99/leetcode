import java.util.PriorityQueue;
import java.util.Random;

public class KthLargestElementInArray {
    public static void main(String[] args) {
        int[][] testcase = {
                {3, 2, 3, 1, 2, 4, 5, 5, 6},
                {3, 2, 1, 5, 6, 4},
                {4, 1, -1, 2, -1, 2, 3},
                {5, 3, 1, 1, 1, 3, 73, 1},
                {5, 2, 5, 3, 5, 3, 1, 1, 3},
                {5, -3, 9, 1, 7, 7, 9, 10, 2, 2, 10, 10, 3, -1, 3, 7, -9, -1, 3, 3},
                {5, 1, -1, -8, -7, 8, -5, 0, 1, 10, 8, 0, -4, 3, -1, -1, 4, -5, 4, -3, 0, 2, 2, 2, 4, -2, -4, 8, -7, -7, 2, -8, 0, -8, 10, 8, -8, -2, -9, 4, -7, 6, 6, -1, 4, 2, 8, -3, 5, -9, -3, 6, -8, -5, 5, 10, 2, -5, -1, -5, 1, -3, 7, 0, 8, -2, -3, -1, -5, 4, 7, -9, 0, 2, 10, 4, 4, -4, -1, -1, 6, -8, -9, -1, 9, -9, 3, 5, 1, 6, -1, -2, 4, 2, 4, -6, 4, 4, 5, -5}

        };
        int k = 2;
        long start = System.nanoTime();
        for (int[] nums : testcase) {
            System.out.println("findKthLargest: " + findKthLargest(nums, k));
        }
        long end = System.nanoTime();

        for (int[] nums : testcase) {
            System.out.println("findKthLargestFastest: " + findKthLargestFastest(nums, k));
        }

        System.out.println("findKthLargest total time: " + (end - start));
        System.out.println("findKthLargestFastest total time: " + (System.nanoTime() - end));
    }

    /**
     * basic
     *
     * @param nums
     * @param k
     * @return
     */
    public static int findKthLargest(int[] nums, int k) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        for (Integer num : nums) {
            if (pq.size() < k) {
                pq.add(num);
            } else {
                assert pq.peek() != null;
                if (num > pq.peek()) {
                    pq.poll();
                    pq.add(num);
                }
            }
        }
        return pq.peek();
    }

    /**
     * fastest
     *
     * @param nums
     * @param k
     * @return
     */
    public static int findKthLargestFastest(int[] nums, int k) {
        return quickSelect(nums, 0, nums.length - 1, nums.length - k);
    }

    /**
     * @param nums  array unsorted number
     * @param left  left index of array
     * @param right right index of array
     * @param k     index of the target
     * @return
     */
    private static int quickSelect(int[] nums, int left, int right, int k) {
        if (left == right) return nums[left];

        int pIndex = new Random().nextInt(right - left + 1) + left;
        pIndex = partition(nums, left, right, pIndex);

        if (pIndex == k) return nums[k];
        else if (pIndex < k) return quickSelect(nums, pIndex + 1, right, k);
        return quickSelect(nums, left, pIndex - 1, k);
    }

    private static int partition(int[] nums, int left, int right, int pIndex) {
        int pivot = nums[pIndex];
        swap(nums, pIndex, right);
        pIndex = left;

        for (int i = left; i <= right; i++)
            if (nums[i] <= pivot)
                swap(nums, i, pIndex++);

        return pIndex - 1;
    }

    private static void swap(int[] nums, int x, int y) {
        int temp = nums[x];
        nums[x] = nums[y];
        nums[y] = temp;
    }
}
