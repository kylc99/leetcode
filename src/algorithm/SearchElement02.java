package algorithm;

import java.util.TreeSet;

public class SearchElement02 {
    public static void main(String[] args) {
        int[][] a = {{6, 5, 4}, {3, 2, 1}, {3, 2, 1}};
        System.out.println(searchElement(a, 7));
    }

    public static int searchElement(int[][] a, int n) {
        TreeSet<Integer> al = new TreeSet<Integer>();
        for (int x[] : a) {
            for (int y : x) {
                al.add(y);
            }
        }
        if (al.size() < n) {
            return -1;
        }
        for (int i = 0; i < n - 1; i++) {
            al.pollFirst();
        }
        return al.pollFirst();

    }

}
