package algorithm;

public class FactorSum {
    public static void main(String[] args) {
        System.out.println(new FactorSum().factorSum(24));
    }

    int factorSum(int n) {
        if (isPrime(n)) {
            return n;
        }
        int sum = 0;
        int tmp = 2;
        while (n != 1) {
            if (n % tmp == 0) {
                sum += tmp;
                n /= tmp;
            } else {
                tmp++;
            }
        }
        return factorSum(sum);
    }

    boolean isPrime(int number) {
        if (number == 4) return true;
        int i;
        for (i = 2; i <= number - 1; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        if (i == number) {
            return true;
        }
        return false;
    }
}
