package algorithm;

public class ReverseNumber {
    public static void main(String[] args) {
        System.out.println(reverseNumber(12345679));
    }

    public static Integer reverseNumber(Integer number) {
        return number < 10 ? number : reverseNumber(number / 10) + (number % 10) * (int) Math.pow(10, number.toString().length() - 1);
    }
}
