package algorithm;

import java.text.Normalizer;
import java.util.regex.Pattern;

/**
 * FileRename
 *
 * @author by kylc on 04/03/2023
 * @project leetcode
 */


public class FileRename {
	public static void main(String[] args) {

//		System.out.println(convertFileName("Xin-chào Việt Nam, Tôi Đi đây.pptx"));

		// Path of folder where files are located
//		String folder_path = "D:\\babySchoolDocs\\backup_file_baby_schools";
//
//		// creating new folder
//		File myfolder = new File(folder_path);
//
//		File[] file_array = myfolder.listFiles();
//		for (int i = 0; i < file_array.length; i++) {
//			if (file_array[i].isFile()) {
//
//				File myfile = new File(folder_path + "\\" + file_array[i].getName());
//				String long_file_name = file_array[i].getName();
//				System.out.println(long_file_name);
//				String new_file_name = convertFileName(long_file_name);
//				System.out.println(new_file_name);
//
//				// file name format: "Snapshot 11 (12-05-2017 11-57).png"
//				// To Shorten it to "11.png", get the substring which
//				// starts after the first space character in the long
//				// _file_name.
//				myfile.renameTo(new File(folder_path +
//						"\\" + new_file_name));
//			}
//		}
	}

	public static String convertFileName(String str) {
		String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(nfdNormalizedString).replaceAll("")
				.toLowerCase()
				.replaceAll("đ", "d")
				.replaceAll("&", "va")
				.replaceAll("[^a-zA-Z0-9.()]", " ").trim()
				.replaceAll("\\s+", "_")
				;
	}
}