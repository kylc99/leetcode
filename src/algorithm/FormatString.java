package algorithm;

public class FormatString {
    public static void main(String[] args) {
        System.out.println(formatString("     fdask   dkfjd   dfjdsjf   dfkdj "));
    }

    public static String formatString(String input) {
        input = input.trim();
        String result = "";
        int count = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == ' ') {
                count++;
                if (count == 1) {
                    result += input.charAt(i);
                }
            } else {
                count = 0;
                result += input.charAt(i);
            }
        }
        return result;
    }
}
