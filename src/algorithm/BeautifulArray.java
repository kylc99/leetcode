package algorithm;

public class BeautifulArray {
    public static void main(String[] args) {
        int[] a = {1, 2, 3, 3};
        System.out.println(new BeautifulArray().beautifulArray(a));
    }

    public boolean beautifulArray(int[] arr) {
        int n = 1;
        while (n < arr.length - 1) {
            if (getSum(arr, 0, n - 1) == getSum(arr, n + 1, arr.length - 1)) {
                return true;
            }
            n++;
        }
        return false;
    }


    public static int[] build(int[] a) {
        int[] S = new int[a.length];
        for (int i = 0; i < a.length; i++) {
            if (i == 0) {
                S[i] = a[i];
            } else {
                S[i] = S[i - 1] + a[i];
            }
        }
        return S;
    }

    public static int getSum(int[] a, int l, int r) {
        int[] S = build(a);
        if (l == 0) {
            return S[r];
        }
        return S[r] - S[l - 1];
    }
}
