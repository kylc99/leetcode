package algorithm;

import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        int target = 6;
        int [] arr = {2, 3, 3,9, 11, 15};
        Class<?> clazz = Class.forName("algorithm.TwoSum");
        System.out.println(((TwoSum) clazz.newInstance()).twoSum(target, arr));

    }
    public String twoSum(int target, int [] arr){
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < arr.length ; i++) {
            int remain = target - arr[i];
            if(map.containsKey(remain)){
                return map.get(remain) + " " + i;
            } else {
                map.put(arr[i], i);
            }
        }
        return null;
    }
}
