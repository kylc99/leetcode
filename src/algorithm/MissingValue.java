package algorithm;

import java.util.TreeSet;

public class MissingValue {
    public static void main(String[] args) {
        int[] a = {7, 2, 5, 3, 5, 3};
        int[] b = {7, 2, 5, 4, 6, 3, 5, 3};
        System.out.println(missingValue(a, b).length);
    }

    public static int[] missingValue(int[] a, int[] b) {
        TreeSet<Integer> al = new TreeSet<Integer>();
        TreeSet<Integer> bl = new TreeSet<Integer>();
        int count = 0;
        for (int i : a) {
            al.add(i);
        }
        for (int i : b) {
            if (!al.contains(i)) {
                bl.add(i);
            }
        }
        int[] result = new int[bl.size()];
        for (int i : bl) {
            result[count++] = i;
        }
        return result;
    }
}
