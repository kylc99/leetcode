package algorithm;

import java.util.Scanner;

public class ReverseString {
    public static boolean isReverseString(String s) {
        return s.length() <= 1 ? true : (s.charAt(0) != s.charAt(s.length() - 1) ? false : isReverseString(s.substring(1, s.length() - 1)));
    }

    public static String reverseString(String myStr) {
        return myStr.isEmpty() ? myStr : (reverseString(myStr.substring(1)) + myStr.charAt(0));
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String A = sc.next();
        //System.out.println( A.equals( new StringBuilder(A).reverse().toString()) ? "Yes" : "No");
        System.out.println(isReverseString(A) ? "Yes" : "No");
        System.out.println(reverseString(A));
    }
}
