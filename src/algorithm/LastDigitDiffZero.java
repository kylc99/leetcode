package algorithm;

import java.math.BigInteger;

public class LastDigitDiffZero {
    int lastDigitDiffZero(int n) {
        BigInteger f = BigInteger.ONE; // Or  new BigInteger("1")
        for (int i = 2; i <= n; i++) {
            f = f.multiply(BigInteger.valueOf(i));
        }
        String str = f.toString();
        char result = '1';
        for (int i = str.length() - 1; i >= 0; i--) {
            if (str.charAt(i) != '0') {
                result = str.charAt(i);
                break;
            }
        }
        return Character.getNumericValue(result);
    }

}
