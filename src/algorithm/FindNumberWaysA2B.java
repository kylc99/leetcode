package algorithm;

public class FindNumberWaysA2B {
    public static class Location {
        private Integer x;
        private Integer y;

        public Location(Integer x, Integer y) {
            this.x = x;
            this.y = y;
        }

        public Integer getX() {
            return x;
        }

        public void setX(Integer x) {
            this.x = x;
        }

        public Integer getY() {
            return y;
        }

        public void setY(Integer y) {
            this.y = y;
        }
    }

    public static void main(String[] args) {
        Location A = new Location(0, 0);
        Location B = new Location(4, 4);
        Location E = new Location(3, 3);
        System.out.println(AToB(A, E));
        System.out.println(AToBNotThroughE(A, B, E));
    }

    public static Integer AToB(Location A, Location B) {
        return (Factorial(B.getX() - A.getX() + B.getY() - A.getY()) / (Factorial(B.getX() - A.getX()) * Factorial(B.getY() - A.getY())));
    }

    public static Integer AToBNotThroughE(Location A, Location B, Location E) {
        return AToB(A, B) - AToB(A, E) * AToB(E, B);
    }

    public static Integer Factorial(Integer n) {
        return n == 1 ? 1 : (Factorial(n - 1) * n);
    }
}

