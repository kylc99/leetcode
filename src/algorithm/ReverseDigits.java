package algorithm;

public class ReverseDigits {
    public static void main(String[] args) {
        System.out.println(reverseNumber(-120));
    }

    private static String reverseNumber(int y) {
        boolean signed = y < 0;
        y = Math.abs(y);
        String s = (new StringBuilder(y + "")).reverse().toString();
        while (s.length() > 1 && s.charAt(0) == '0') {
            s = s.substring(1);
        }
        if (s.equals("0")) {
            return "0";
        } else {
            return (signed ? "-" : "") + s;
        }
    }
}
