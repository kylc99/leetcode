package algorithm;

import java.util.Arrays;
import java.util.stream.Collectors;

public class CheckEqualFrequency {
    public static void main(String[] args) {
        int[] inputArray = {1, 1};
        System.out.println(checkEqualFrequency(inputArray));
    }

    // pass 10/11
    static boolean checkEqualFrequency(int[] inputArray) {
        Arrays.sort(inputArray);
        int size = inputArray.length;
        if (size == 1) return true;
        int count = 0;
        int index = 0;
        while (index < size && inputArray[0] == inputArray[index++]) {
            count++;
        }
        if (size % count != 0) return false;
        for (int i = count; i < size; i += count) {
            if (inputArray[i] != inputArray[i + count - 1]) return false;
        }
        return true;
    }

    // pass 11/11
    static boolean checkEqualFrequency1(int[] inputArray) {
        return Arrays.stream(inputArray)
                .boxed()
                .collect(Collectors.groupingBy(Integer::intValue, Collectors.counting()))
                .values()
                .stream()
                .distinct()
                .count() == 1L;
    }
}
