package algorithm;

public class MostCharacter {
    public static void main(String[] args) {
        System.out.println(getMostCharacter("aaabbbbcca"));
    }

    public static char getMostCharacter(String str) {
        int[] arr = new int[256];
        int max, index;
        max = index = 0;
        for (int i = 0; i < str.length(); i++) {
            arr[str.charAt(i)]++;
            if (max < arr[str.charAt(i)]) {
                max = arr[str.charAt(i)];
                index = i;
            } else if (max == arr[str.charAt(i)]) {
                if (str.indexOf(str.charAt(i)) < str.indexOf(str.charAt(index))) {
                    index = i;
                }
            }
        }
        return str.charAt(index);
    }
}
