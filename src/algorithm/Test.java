package algorithm;

import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.stream.IntStream;

public class Test {
    public static void main(String[] args) {
        // Function to merge two arrays in Java 8
        int[] a = {1, 3};
        int[] b = {2};
        System.out.println(findMedianSortedArrays1(a, b));
    }

    public static double findMedianSortedArrays1(int[] nums1, int[] nums2) {
        Integer[] a = Arrays.stream(IntStream.concat(Arrays.stream(nums1),
                                Arrays.stream(nums2))
                        .toArray())
                .boxed()
                .toArray(Integer[]::new);

        int n = a.length;
        PriorityQueue<Integer> maxPQ = new PriorityQueue(n / 2 + 1, Collections.reverseOrder());
        PriorityQueue<Integer> minPQ = new PriorityQueue(n / 2 + 1);
        for (int i = 0; i < n; i++) {
            if (maxPQ.isEmpty()) {
                maxPQ.add(a[i]);
            } else if (maxPQ.size() == minPQ.size()) {
                if (a[i] < minPQ.peek()) {
                    maxPQ.add(a[i]);
                } else {
                    minPQ.add(a[i]);
                    maxPQ.add(minPQ.poll());
                }
            } else if (maxPQ.size() > minPQ.size()) {
                if (a[i] > maxPQ.peek()) {
                    minPQ.add(a[i]);
                } else {
                    maxPQ.add(a[i]);
                    minPQ.add(maxPQ.poll());
                }
            }

        }
        if (maxPQ.size() == minPQ.size()) {
            return (maxPQ.peek() + minPQ.peek()) / 2.0;
        } else {
            return maxPQ.peek();
        }
    }
}
