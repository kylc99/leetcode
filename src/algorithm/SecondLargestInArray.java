package algorithm;

public class SecondLargestInArray {
    public static void main(String[] args) {
        int[] arr = {2, 5, 7, 1, 18, 20, 17, 15, 19};
        int[] arr1 = {10, 10, 10};
        Long startTime = System.nanoTime();
        System.out.println("1IF: " + secondLargestInArray_1IF(arr));
        Long middleTime = System.nanoTime();
        System.out.println("2IF: " + secondLargestInArray_2IF(arr1));
        Long endTime = System.nanoTime();
        System.out.println("1IF: " + (middleTime - startTime) + "\n2IF: " + (endTime - middleTime));
    }

    public static String secondLargestInArray_1IF(int[] arr) {
        int max = Integer.MIN_VALUE;
        int secondLargest = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            secondLargest = ((secondLargest + arr[i]) + Math.abs(secondLargest - arr[i])) / 2;
            if (arr[i] > max) {
                secondLargest = max;
                max = arr[i];
            }
        }
        return secondLargest == max ? "not exist" : secondLargest + "";
    }

    public static String secondLargestInArray_2IF(int[] arr) {
        int max = Integer.MIN_VALUE;
        int secondLargest = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                secondLargest = max;
                max = arr[i];
            } else if (arr[i] > secondLargest) {
                secondLargest = arr[i];
            }
        }
        return secondLargest == max ? "not exist" : secondLargest + "";
    }

    public static Integer max2(int number1, int number2) {
        return ((number1 + number2) + Math.abs(number2 - number1)) / 2;
    }
}
